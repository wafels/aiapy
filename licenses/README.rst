Licenses
========

This directory holds license and credit information for works aiapy is derived
from, and/or datasets.

The license file for the aiapy package itself is placed in the root directory
of this repository as LICENSE.rst.
